package br.com.eprecise.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.eprecise.assembler.InputDisassembler;
import br.com.eprecise.assembler.ModelAssembler;
import br.com.eprecise.dto.CidadeDTO;
import br.com.eprecise.dto.EstadoDTO;
import br.com.eprecise.dto.input.EstadoInput;
import br.com.eprecise.model.Estado;
import br.com.eprecise.repository.EstadoRepository;
import br.com.eprecise.service.EstadoService;

@Path("/estados")
@Produces(MediaType.APPLICATION_JSON)
public class EstadoResource {

	@Inject
	private EstadoService estadoService;
	
	@Inject
	private EstadoRepository estadoRepository;
	
	@Inject
	private ModelAssembler modelAssembler;
	
	@Inject
	private InputDisassembler inputDisassembler;
	
	@GET
	public List<EstadoDTO> listarEstados(@QueryParam("page") Integer page, @QueryParam("size") Integer size, @QueryParam("nome") String nome) {
		Map<String, Object> parametros = new HashMap<>();
		addIfNotNull(parametros, "page", page);
		addIfNotNull(parametros, "size", size);
		addIfNotNull(parametros, "nome", nome);
		
		if (parametros.isEmpty()) {			
			return modelAssembler.toCollectionModel(estadoRepository.listAll(), CidadeDTO.class);
		}
		
		if (!parametros.containsKey("nome")) {
			return modelAssembler.toCollectionModel(estadoRepository.listAll(page, size), CidadeDTO.class);
		}
		
		return modelAssembler.toCollectionModel(estadoRepository.listAll(nome, page, size), CidadeDTO.class);
    }
	
	@GET
	@Path("/{estadoId}")
	public EstadoDTO buscar(@PathParam("estadoId") Long estadoId) {
		return modelAssembler.toModel(estadoService.buscarOuFalhar(estadoId), EstadoDTO.class);
	}
	
	@GET
	@Path("/sigla/{siglaEstado}")
	public EstadoDTO buscarPorSigla(@PathParam("siglaEstado") String siglaEstado) {
		return modelAssembler.toModel(estadoService.buscarPorSiglaOuFalhar(siglaEstado.toUpperCase()), EstadoDTO.class);
	}
	
	@GET
	@Path("/total_registros")
	public Long totalDeRegistrosSalvos() {
		return estadoRepository.listSize();
	}
	
	@POST
	public Response salvar(@RequestBody @Valid EstadoInput estadoInput) {
		Estado estado = inputDisassembler.toDomainObject(estadoInput, Estado.class);
		
		return Response.ok(modelAssembler.toModel(estadoService.salvar(estado), EstadoDTO.class))
				.status(Response.Status.CREATED).build();
	}
	
	@PUT
	@Path("/{estadoId}")
	public Response atualizar(@RequestBody @Valid EstadoInput estadoInput, @PathParam("estadoId") Long estadoId) {
		Estado estadoAtual = estadoService.buscarOuFalhar(estadoId);
		
		inputDisassembler.copyToDomainObject(estadoInput, estadoAtual);
		
		return Response.ok(modelAssembler.toModel(estadoService.atualizar(estadoAtual, estadoId), EstadoDTO.class))
				.status(Response.Status.OK).build();
	}
	
	@DELETE
	@Path("/{estadoId}")
	//@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathParam("estadoId") Long estadoId) {
		estadoService.excluir(estadoId);
	}
	
	private static void addIfNotNull(Map<String, Object> map, String key, Object value) {
		if (value != null) {
			map.put(key, value);
		}
	}
}