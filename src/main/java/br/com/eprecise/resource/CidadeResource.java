package br.com.eprecise.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.eprecise.assembler.InputDisassembler;
import br.com.eprecise.assembler.ModelAssembler;
import br.com.eprecise.dto.CidadeDTO;
import br.com.eprecise.dto.input.CidadeInput;
import br.com.eprecise.model.Cidade;
import br.com.eprecise.model.Estado;
import br.com.eprecise.repository.CidadeRepository;
import br.com.eprecise.service.CidadeService;
import br.com.eprecise.service.EstadoService;

@Path("/cidades")
@Produces(MediaType.APPLICATION_JSON)
public class CidadeResource {
	
	@Inject
	private CidadeRepository cidadeRepository;
	
	@Inject
	private CidadeService cidadeService;
	
	@Inject
	private EstadoService estadoService;
	
	@Inject
	private ModelAssembler modelAssembler;
	
	@Inject
	private InputDisassembler inputDisassembler;
	
	@GET
    public List<CidadeDTO> listarCidades(@QueryParam("page") Integer page, @QueryParam("size") Integer size, @QueryParam("nome") String nome) {
		Map<String, Object> parametros = new HashMap<>();
		addIfNotNull(parametros, "page", page);
		addIfNotNull(parametros, "size", size);
		addIfNotNull(parametros, "nome", nome);
		
		if (parametros.isEmpty()) {			
			return modelAssembler.toCollectionModel(cidadeRepository.listAll(), CidadeDTO.class);
		}
		
		if (!parametros.containsKey("nome")) {
			return modelAssembler.toCollectionModel(cidadeRepository.listAll(page, size), CidadeDTO.class);
		}
		
		return modelAssembler.toCollectionModel(cidadeRepository.listAll(nome, page, size), CidadeDTO.class);
    }
    
	@GET
	@Path("/{cidadeId}")
    public CidadeDTO buscar(@PathParam("cidadeId") Long cidadeId) {
    	return modelAssembler.toModel(cidadeService.buscarOuFalhar(cidadeId), CidadeDTO.class);
    }
	
	@GET
	@Path("/estados/{sigla}")
	public List<CidadeDTO> listarCidadesPorEstado(@QueryParam("page") Integer page, @QueryParam("size") Integer size, @PathParam("sigla") String sigla) {		
		Estado estado = estadoService.buscarPorSiglaOuFalhar(sigla.toUpperCase());
		
		return modelAssembler.toCollectionModel(cidadeRepository.findByEstado(estado.getId(), page, size), CidadeDTO.class);
	}
	
	@GET
	@Path("/total_registros")
	public Long totalDeRegistrosSalvos() {
		return cidadeRepository.listSize();
	}
    
	@POST
    public Response salvar(@RequestBody @Valid CidadeInput cidadeInput) {
		Estado estado = estadoService.buscarOuFalhar(cidadeInput.getEstado().getId());
		
		Cidade cidade = inputDisassembler.toDomainObject(cidadeInput, Cidade.class);
		
		cidade.setEstado(estado);
		
		return Response.ok(modelAssembler.toModel(cidadeService.salvar(cidade), CidadeDTO.class))
				.status(Response.Status.CREATED).build();
    }
    
	@PUT
	@Path("/{cidadeId}")
    public Response atualizar(@RequestBody @Valid CidadeInput cidadeInput, @PathParam("cidadeId") Long cidadeId) {
		Estado estado = estadoService.buscarOuFalhar(cidadeInput.getEstado().getId());
		
		Cidade cidadeAtual = cidadeService.buscarOuFalhar(cidadeId);
		
		inputDisassembler.copyToDomainObject(cidadeInput, cidadeAtual);
		
		cidadeAtual.setEstado(estado);
		
		return Response.ok(modelAssembler.toModel(cidadeService.atualizar(cidadeAtual, cidadeId), CidadeDTO.class))
				.status(Response.Status.OK).build();
    }
    
	@DELETE
	@Path("/{cidadeId}")
    public void excluir(@PathParam("cidadeId") Long cidadeId) {
    	cidadeService.excluir(cidadeId);
    }
	
	private static void addIfNotNull(Map<String, Object> map, String key, Object value) {
		if (value != null) {
			map.put(key, value);
		}
	}
}
