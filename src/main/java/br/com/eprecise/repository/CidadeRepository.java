package br.com.eprecise.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.eprecise.model.Cidade;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class CidadeRepository implements PanacheRepository<Cidade> {
	
	public List<Cidade> listAll(String nome, Integer page, Integer size) {
		String searchInput = "%" + nome + "%";
		
		PanacheQuery<Cidade> listaCidades = find("nome like ?1", searchInput);
		
		if (page == null || page < 0) {
			page = 0;
		}
		
		if (size == null || size < 0) {
			size = (int) count();
		}
		
		return listaCidades.page(Page.of(page, size)).list();
	}
	
	public List<Cidade> listAll(Integer page, Integer size) {
		PanacheQuery<Cidade> listaCidades = findAll();
		
		if (page == null || page < 0) {
			page = 0;
		}
		
		if (size == null || size <= 0) {
			size = (int) listSize();
		}
		
		return listaCidades.page(Page.of(page, size)).list();
	}
	
	public List<Cidade> listAll(String nome) {
		String searchInput = "%" + nome + "%";
		
		PanacheQuery<Cidade> listaCidades = find("nome like ?1", searchInput);
		
		return listaCidades.list();
	}
	
	public List<Cidade> listAll() {
		PanacheQuery<Cidade> listaCidades = findAll();
		
		return listaCidades.list();
	}
	
	public boolean findByEstado(Long id) {
		Cidade cidade = find("estado_id = :id", Parameters.with("id", id)).firstResult();
		
		if (cidade == null) return false; 
		
		return true;
	}
	
	public List<Cidade> findByEstado(Long id, Integer page, Integer size) {
		PanacheQuery<Cidade> listaCidades = find("estado_id = :id", Parameters.with("id", id));
		
		if (page == null || page < 0) {
			page = 0;
		}
		
		System.out.println(size);
		
		if (size == null || size <= 0) {
			size = (int) listaCidades.count();
		}
		
		return listaCidades.page(Page.of(page, size)).list();
	}
	
	public Optional<Cidade> findByIdOptional(Long id) {
		return Optional.ofNullable(findById(id));
	}
	
	public Cidade save(Cidade cidade) {
		persist(cidade);
		return cidade;
	}
	
	public boolean deleteById(Long id) {
		Cidade cidade = findById(id);
		
		delete(cidade);
		
		return true;
	}
	
	public long listSize() {
		return count();
	}
}