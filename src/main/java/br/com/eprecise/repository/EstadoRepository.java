package br.com.eprecise.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.eprecise.model.Estado;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;

/*@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>, EstadoRepositoryQuery {

}*/

@ApplicationScoped
public class EstadoRepository implements PanacheRepository<Estado> {
	
	public List<Estado> listAll(String nome, Integer page, Integer size) {
		String searchInput = "%" + nome + "%";
		
		PanacheQuery<Estado> listaEstados = find("nome like ?1", searchInput);
		
		if (page == null || page < 0) {
			page = 0;
		}
		
		if (size == null || size < 0) {
			size = (int) count();
		}
		
		return listaEstados.page(Page.of(page, size)).list();
	}
	
	public List<Estado> listAll(Integer page, Integer size) {
		PanacheQuery<Estado> listaEstados = findAll();
		
		if (page == null || page < 0) {
			page = 0;
		}
		
		if (size == null || size < 0) {
			size = (int) listSize();
		}
		
		return listaEstados.page(Page.of(page, size)).list();
	}
	
	public List<Estado> listAll(String nome) {
		String searchInput = "%" + nome + "%";
		
		PanacheQuery<Estado> listaEstados = find("nome like ?1", searchInput);
		
		return listaEstados.list();
	}
	
	public List<Estado> listAll() {
		PanacheQuery<Estado> listaEstados = findAll();
		
		return listaEstados.list();
	}
	
	public Optional<Estado> findByIdentity(Long id) {
		return find("id", id).firstResultOptional();
	}
	
	public Optional<Estado> findBySigla(String sigla) {
		return find("sigla", sigla).firstResultOptional();
	}
	
	public Estado save(Estado estado) {
		persist(estado);
		return estado;
	}
	
	public boolean deleteById(Long id) {
		Estado estado = findById(id);
		
		delete(estado);
		
		return true;
	}
	
	public long listSize() {
		return count();
	}
}
