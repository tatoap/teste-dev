package br.com.eprecise.service;

import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import br.com.eprecise.exception.EntidadeEmUsoException;
import br.com.eprecise.exception.EntidadeJaExistenteException;
import br.com.eprecise.exception.EntidadeNaoEncontradaException;
import br.com.eprecise.exception.NegocioException;
import br.com.eprecise.model.Estado;
import br.com.eprecise.repository.CidadeRepository;
import br.com.eprecise.repository.EstadoRepository;

@Service
public class EstadoService {
	
	private static final String MSG_SIGLA_ESTADO_NAO_ENCONTRADO = "Estado de sigla %s não foi encontrado.";
	private static final String MSG_ESTADO_NAO_ENCONTRADO = "Estado de id %d não foi encontrado.";
	private static final String MSG_ESTADO_JA_CADASTRADO = "Sigla de estado '%s' já esta cadastrado no sistema.";
	private static final String MSG_ESTADO_EM_USO = "Estado de id %d esta em uso e não pode ser excluído.";
	
	@Inject
	private EstadoRepository estadoRepository;
	
	@Inject
	private CidadeRepository cidadeRepository;
	
	@Transactional
	public Estado salvar(Estado estado) {
		Optional<Estado> verificarEstado = estadoRepository.findBySigla(estado.getSigla());
		
		if (verificarEstado.isPresent()) {
			throw new EntidadeJaExistenteException(String.format(MSG_ESTADO_JA_CADASTRADO, estado.getSigla()));
		}
		
		return estadoRepository.save(estado);
	}
	
	@Transactional
	public Estado atualizar(Estado estado, Long estadoId) {
		Estado estadoAtual = buscarOuFalhar(estadoId);
		
		estadoAtual.setNome(estado.getNome());
		
		estadoAtual.setSigla(estado.getSigla());
				
		return estadoAtual;
	}
	
	@Transactional
	public void excluir(Long estadoId) {
		buscarOuFalhar(estadoId);
				
		if (temCidade(estadoId)) {
			
			throw new EntidadeEmUsoException(String.format(MSG_ESTADO_EM_USO, estadoId));
		} 
		
		estadoRepository.deleteById(estadoId);
		estadoRepository.flush();
	}
	
	public Boolean temCidade(Long estadoId) {		
		return cidadeRepository.findByEstado(estadoId);
	}
	
	public Estado buscarPorSiglaOuFalhar(String sigla) {
		return estadoRepository.findBySigla(sigla.toUpperCase())
				.orElseThrow(() -> new NegocioException(String.format(MSG_SIGLA_ESTADO_NAO_ENCONTRADO, sigla)));
	}
	
	public Estado buscarOuFalhar(Long estadoId) {
		return estadoRepository.findByIdOptional(estadoId)
				.orElseThrow(() -> new EntidadeNaoEncontradaException(String.format(MSG_ESTADO_NAO_ENCONTRADO, estadoId)));
		
	}
}
