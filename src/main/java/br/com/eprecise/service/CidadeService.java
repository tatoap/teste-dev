package br.com.eprecise.service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.eprecise.exception.EntidadeNaoEncontradaException;
import br.com.eprecise.model.Cidade;
import br.com.eprecise.repository.CidadeRepository;

@Service
public class CidadeService {
	
	private static final String MSG_CIDADE_NAO_ENCONTRADA = "Cidade de id %d não foi encontrada.";
	private static final String MSG_NOME_REQUERIDO = "Nome da cidade é obrigatório.";

	@Inject
	private CidadeRepository cidadeRepository;
		
	@Transactional
	public Cidade salvar(@RequestBody Cidade cidade) {
		if (cidade.getNome().isEmpty() || cidade.getNome() == null) {
			System.out.println("PASSEI AQUI");
			
			throw new BadRequestException(MSG_NOME_REQUERIDO);
		}
		
		return cidadeRepository.save(cidade);
	}
	
	@Transactional
	public void excluir(Long cidadeId) {
		buscarOuFalhar(cidadeId);
		
		cidadeRepository.deleteById(cidadeId);
	}
	
	@Transactional
	public Cidade atualizar(Cidade cidade, Long cidadeId) {
		Cidade cidadeAtual = buscarOuFalhar(cidadeId);
		
		cidadeAtual.setNome(cidade.getNome());
		
		cidadeAtual.setEstado(cidade.getEstado());
				
		return cidadeAtual;
	}
	
	public Cidade buscarOuFalhar(Long cidadeId) {
		return cidadeRepository.findByIdOptional(cidadeId)
				.orElseThrow(() -> new EntidadeNaoEncontradaException(String.format(MSG_CIDADE_NAO_ENCONTRADA, cidadeId)));
	}
}
