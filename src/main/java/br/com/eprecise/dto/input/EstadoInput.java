package br.com.eprecise.dto.input;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class EstadoInput {
	
	@NotBlank
	private String nome;
	
	@Size(min = 2, max = 2)
	@NotBlank
	private String sigla;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
}
