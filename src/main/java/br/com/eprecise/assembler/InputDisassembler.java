package br.com.eprecise.assembler;

import java.lang.reflect.Type;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class InputDisassembler {

	@Inject
	private ModelMapper modelMapper;
	
	public <E, T> E toDomainObject(T entityInput, Type model) {
		return modelMapper.map(entityInput, model);
	}
	
	public <E, T> void copyToDomainObject(T entityInput, Object destination) {
		modelMapper.map(entityInput, destination);
	}
	
}
