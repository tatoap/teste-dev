package br.com.eprecise.assembler;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ModelAssembler {

	@Inject
	private ModelMapper modelMapper;
	
	public <E, T> E toModel(T entity, Type model) {
			return modelMapper.map(entity, model);
	}
	
	@SuppressWarnings("unchecked")
	public <E, T> List<E> toCollectionModel(List<T> entity, Type model) {
		return (List<E>) entity.stream()
				.map(t -> toModel(t, model))
				.collect(Collectors.toList());
	}
}
