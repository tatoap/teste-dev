package br.com.eprecise.configuration.modelmapper;

import javax.enterprise.context.ApplicationScoped;

import org.modelmapper.ModelMapper;

@ApplicationScoped
public class ModelMapperConfig {
	
	@ApplicationScoped
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
