package br.com.eprecise.exception;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;

@Provider
public class GeneralException implements ExceptionMapper<Exception> {
	
	MessagemErro mensagemErro = new MessagemErro();

	@Override
	@Produces(MediaType.APPLICATION_JSON)
	public Response toResponse(Exception e) {
				
		if (e instanceof NegocioException) {
			mensagemErro.setMessageError(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).entity(mensagemErro).build();
		}
		
		if (e instanceof EntidadeNaoEncontradaException) {
			mensagemErro.setMessageError(e.getMessage());
			return Response.status(Response.Status.NOT_FOUND).entity(mensagemErro).build();
		}
		
		if (e instanceof EntidadeEmUsoException) {
			mensagemErro.setMessageError(e.getMessage());
			return Response.status(Response.Status.CONFLICT).entity(mensagemErro).build();
		}
		
		if (e instanceof EntidadeJaExistenteException) {
			mensagemErro.setMessageError(e.getMessage());
			return Response.status(Response.Status.CONFLICT).entity(mensagemErro).build();
		}
		
		if (e instanceof MismatchedInputException) {
			mensagemErro.setMessageError("O parametro recebido é inválido. Por favor, "
					+ "verifique os valores enviados.");
			return Response.status(Response.Status.CONFLICT).entity(mensagemErro).build();
		}
		
		mensagemErro.setMessageError("Erro: Por favor, entre em contato com o suporte.");
		
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity(mensagemErro).build();
	}

}
