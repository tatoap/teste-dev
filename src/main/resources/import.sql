insert into estado (nome, sigla) values ('São Paulo', 'SP');
insert into estado (nome, sigla) values ('Minas Gerais', 'MG');
insert into estado (nome, sigla) values ('Espírito Santo', 'ES');
insert into estado (nome, sigla) values ('Rio de Janeiro', 'RJ');
insert into estado (nome, sigla) values ('Ceará', 'CE');

insert into cidade (nome, estado_id) values ('Guarulhos', 1);
insert into cidade (nome, estado_id) values ('Teófilo Otoni', 2);
insert into cidade (nome, estado_id) values ('Vitória', 3);
insert into cidade (nome, estado_id) values ('Volta Redonda', 4);
insert into cidade (nome, estado_id) values ('Campinas', 1);