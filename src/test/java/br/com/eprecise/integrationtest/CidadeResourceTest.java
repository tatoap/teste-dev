package br.com.eprecise.integrationtest;

import static io.restassured.RestAssured.given;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.google.common.net.HttpHeaders;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CidadeResourceTest {

	@Test
	public void deveRetornar201_QuandoCadastrarUmaCidade() {
		given()
			.body("{\"nome\": \"TESTE\", \"estado\": 1}")
			.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
			.when()
				.post("/cidades")
			.then()
				.statusCode(Status.CREATED.getStatusCode());
			
	}
	
	@Test
	public void deveRetornar404_QuandoCadastrarUmaCidadeDeEstadoInexistente() {
		given()
		.body("{\"nome\": \"TESTE\", \"estado\": 10000}")
		.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
		.when()
			.post("/cidades")
		.then()
			.statusCode(Status.NOT_FOUND.getStatusCode());
	}
	
	@Test
	public void deveRetornar200_QuandoConsultarListaDeCidades() {
		given()
			.when()
				.get("/cidades")
			.then()
				.statusCode(Status.OK.getStatusCode());
	}
	
	@Test
	public void deveRetornar200_QuandoConsultarUmaCidade() {
		given()
			.when()
				.get("/cidades/1")
			.then()
				.statusCode(Status.OK.getStatusCode());
	}
	
	@Test
	public void deveRetornar404_QuandoConsultarUmaCidadeInexistente() {
		given()
			.when()
				.get("/cidades/10000")
			.then()
				.statusCode(Status.NOT_FOUND.getStatusCode());
	}
	
	@Test
	public void deveRetornar204_QuandoExcluirUmaCidade() {
		given()
			.when()
				.delete("/cidades/5")
			.then()
				.statusCode(Status.NO_CONTENT.getStatusCode());
	}
}
