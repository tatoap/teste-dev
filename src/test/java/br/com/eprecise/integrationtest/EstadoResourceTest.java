package br.com.eprecise.integrationtest;

import static io.restassured.RestAssured.given;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.google.common.net.HttpHeaders;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EstadoResourceTest {

	@Test
	public void deveRetornar201_QuandoCadastrarUmEstado() {
		given()
			.body("{\"nome\": \"TESTE\", \"sigla\": \"XX\"}")
			.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
			.when()
				.post("/estados")
			.then()
				.statusCode(Status.CREATED.getStatusCode());
			
	}
	
	@Test
	public void deveRetornar200_QuandoConsultarListaDeEstados() {
		given()
			.when()
				.get("/estados")
			.then()
				.statusCode(Status.OK.getStatusCode());
	}
	
	@Test
	public void deveRetornar200_QuandoConsultarUmEstado() {
		given()
			.when()
				.get("/estados/1")
			.then()
				.statusCode(Status.OK.getStatusCode());
	}
	
	@Test
	public void deveRetornar404_QuandoConsultarUmEstadoInexistente() {
		given()
			.when()
				.get("/estados/10000")
			.then()
				.statusCode(Status.NOT_FOUND.getStatusCode());
	}
	
	@Test
	public void deveRetornar204_QuandoExcluirUmEstado() {
		given()
			.when()
				.delete("/estados/5")
			.then()
				.statusCode(Status.NO_CONTENT.getStatusCode());
	}
	
	@Test
	public void deveRetornar409_QuandoTentaExcluirUmEstadoEmUso() {
		given()
			.when()
				.delete("/estados/1")
			.then()
				.statusCode(Status.CONFLICT.getStatusCode());
	}
}
